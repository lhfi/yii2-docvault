<?php

namespace diggindata\docvault\models;

use Yii;
use Da\User\Model\User;

/**
 * This is the model class for table "{{%permission}}".
 *
 * @property integer $id
 * @property integer $documentId
 * @property integer $userId
 * @property integer $rights
 *
 * @property Document $document
 */
class Permission extends \yii\db\ActiveRecord
{
    Const RIGHT_VIEW = 1;
    Const RIGHT_MODIFY = 2;

    public $deleteMe;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dv_permission}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['documentId','userId','rights'], 'required'],
            [['documentId', 'userId', 'rights'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('docvault', 'ID (PK)'),
            'documentId' => Yii::t('docvault', 'Document'),
            'userId' => Yii::t('docvault', 'User'),
            'rights' => Yii::t('docvault', 'Rights'),
            'deleteMe' => Yii::t('docvault', 'Remove'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'documentId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    public static function getRightsOptions()
    {
        return [
            self::RIGHT_VIEW => Yii::t('docvault', 'View'),
            self::RIGHT_MODIFY => Yii::t('docvault', 'Modify'),
        ];
    }
    
}
