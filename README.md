# Yii2-docvault

Some web applications require a module to manage documents for logged in users.
The current version includes the folowing features:

* Manage document categories
* Manage documents  
  * Upload a new document
  * Set view/modify permissions
  * Check out a document
  * Check in a document
  * View modification logs

## Documentation

[Installation instructions](docs/INSTALL.md) | [Definitive guide to Yii2-docvault](docs/README.md)

Yii2-user is released under the MIT License. See the bundled [LICENSE.md](LICENSE.md) for details.
