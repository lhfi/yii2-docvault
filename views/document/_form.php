<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use diggindata\docvault\models\DocumentCategory;

/* @var $this yii\web\View */
/* @var $model app\models\Document */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="document-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'categoryId')->dropDownList(ArrayHelper::map(DocumentCategory::find()->all(), 'id', 'name'), array('prompt'=>Yii::t('docvault', '(Select)'))) ?>

    <?= '' //$form->field($model, 'ownerId')->textInput() ?>

    <?= '' // $form->field($model, 'realname')->textInput(['maxlength' => 255]) ?>

    <?= '' // $form->field($model, 'created')->textInput() ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= '' // $form->field($model, 'status')->textInput() ?>
    
    <?php if($model->isNewRecord) : ?>
    <?= $form->field($model, 'file')->fileInput() ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('docvault', 'Create') : Yii::t('docvault', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
