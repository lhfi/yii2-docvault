<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use diggindata\docvault\widgets\TileView;

use yii\helpers\Url;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('docvault', 'Documents');
$this->params['breadcrumbs'][] = ['label' => 'DocVault', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$createLink = ['create'];
if (!empty($_GET['DocumentSearch']['categoryId']))
	$createLink['Document[categoryId]'] = $_GET['DocumentSearch']['categoryId'];

/**
* create action column template depending acces rights
*/
$actionColumnTemplates = [];

if (\Yii::$app->user->can('app_sheet_view', ['route' => true])) {
	$actionColumnTemplates[] = '{view}';
}

if (\Yii::$app->user->can('app_sheet_update', ['route' => true])) {
	$actionColumnTemplates[] = '{update}';
}

if (\Yii::$app->user->can('app_sheet_delete', ['route' => true])) {
	$actionColumnTemplates[] = '{delete}';
}
if (isset($actionColumnTemplates)) {
	$actionColumnTemplate = implode(' ', $actionColumnTemplates);
	$actionColumnTemplateString = $actionColumnTemplate;
} else {
	Yii::$app->view->params['pageButtons'] = Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('cruds', 'New'), ['create'], ['class' => 'btn btn-success']);
	$actionColumnTemplateString = "{view} {update} {delete}";
}
$actionColumnTemplateString = '<div class="action-buttons">'.$actionColumnTemplateString.'</div>';
?>

<div class="document-index">

<h1><?= Html::encode($this->title) ?></h1>
<?php echo $this->render('_search', ['model' => $searchModel]); ?>

<p>
	<?= Html::a(Yii::t('docvault', 'Create New Document', ['modelClass' => 'Document']), $createLink, ['class' => 'btn btn-success']) ?>
	<?= Html::a('<span class="glyphicon glyphicon-search"></span> ' . Yii::t('docvault', 'Advanced Search'), '#', ['class' => 'btn btn-primary search-btn']) ?>
</p>

<?php /* echo TileView::widget([
'dataProvider' => $dataProvider,
'itemOptions' => ['class' => 'item col-sm-4'],
'itemView' => function ($model, $key, $index, $widget) {
return $this->render('_view', ['model'=>$model, 'key'=>$key, 'index'=>$index, ]);
},
])*/ ?>

<div class="table-responsive">
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'pager' => [
			'class' => yii\widgets\LinkPager::className(),
			'firstPageLabel' => Yii::t('cruds', 'First'),
			'lastPageLabel' => Yii::t('cruds', 'Last'),
		],
		'filterModel' => \Yii::$app->user->can('Administrator') ? $searchModel : NULL,
		'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
		'headerRowOptions' => ['class'=>'x'],
		'columns' => [
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => $actionColumnTemplateString,
				'buttons' => [
					'view' => function ($url, $model, $key) {
						$options = [
							'title' => Yii::t('cruds', 'View'),
							'aria-label' => Yii::t('cruds', 'View'),
							'data-pjax' => '0',
						];
						return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
					}
				],
				'urlCreator' => function($action, $model, $key, $index) {
					// using the column name as key, not mapping to 'id' like the standard generator
					$params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
					$params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
					return Url::toRoute($params);
				},
				'contentOptions' => ['nowrap'=>'nowrap']
			],
			//'id',
			//'realname',
								[
									'class' => yii\grid\DataColumn::className(),
									'attribute' => 'realname',
									'label' => 'Document',
									'value' => function ($model) {
											return Html::a($model->realname, ['view-file','id'=>$model->id], ['data-pjax' => 0]);
									},
									'format' => 'raw',
								],
			'description',
			'category.name',
			'ownerId.name',
			'created'
		],]) 
	?>
</div>

<?php                               
$script = <<< JS
$('.search-btn').on('click', function(e) {
    $('.document-search-form').toggle(400);
    return false;
});
JS;
$this->registerJs($script);
// where $position can be View::POS_READY (the default), 
// or View::POS_HEAD, View::POS_BEGIN, View::POS_END
// ?>
