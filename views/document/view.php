<?php

use yii\bootstrap\ButtonGroup;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Document */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'DocVault', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('docvault', 'Documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="document-view">


	<h1><?= Html::encode($this->title) ?></h1>


	<?php echo ($model->mayView) ? Html::a('<span class="glyphicon glyphicon-search"></span> '.Yii::t('docvault', 'View'), ['view-file', 'id' => $model->id], ['class' => 'btn btn-info']) : '' ?>
	<?php echo ($model->status==0 and $model->mayModify) ? Html::a('<span class="glyphicon glyphicon-export"></span> '.Yii::t('docvault', 'Check Out'), ['checkout', 'id' => $model->id], ['class' => 'btn btn-success']) : '' ?>
	<?php echo ($model->status==Yii::$app->user->id) ? Html::a('<span class="glyphicon glyphicon-import"></span> '.Yii::t('docvault', 'Check In'), ['checkin', 'id' => $model->id], ['class' => 'btn btn-success']) : '' ?>
	<?php echo ($model->status==0 and $model->mayModify) ? Html::a('<span class="glyphicon glyphicon-pencil"></span> '.Yii::t('docvault', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
	<?php echo ($model->status==0 and $model->mayModify) ? Html::a('<span class="glyphicon glyphicon-pencil"></span> '.Yii::t('docvault', 'Permissions'), ['update-permissions', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
	<?php echo ($model->ownerId == Yii::$app->user->id or $model->mayModify) ? Html::a('<span class="glyphicon glyphicon-remove"></span> ' . Yii::t('docvault', 'Delete'), ['delete', 'id' => $model->id], [
		'class' => 'btn btn-danger',
		'data' => [
			'confirm' => Yii::t('docvault', 'Are you sure you want to delete this item?'),
			'method' => 'post',
		],
	]) : '' ?>

	<?php \yii\widgets\Pjax::begin(['id'=>'pjax-main', 'enableReplaceState'=> false, 'linkSelector'=>'#pjax-main ul.pagination a, th a', 'clientOptions' => ['pjax:success'=>'function(){alert("yo")}']]) ?>


	<?php echo yii\bootstrap\Tabs::widget([
		'items' => [
			[
				'label' => Yii::t('docvault','File'),
				'content' => $this->render('_meta', ['model'=>$model]),
				'active' => true
			],
			[
				'label' => Yii::t('docvault','History'),
				'content' => $this->render('_logs', ['model'=>$model]),
			],
			[
				'label' => Yii::t('docvault', 'Permissions'),
				'content' => $this->render('_permissions', ['model'=>$model]),
			],
		],
	]); ?>

</div>

<?php \yii\widgets\Pjax::end(); ?>



