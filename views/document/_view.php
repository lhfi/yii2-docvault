<?php
use yii\helpers\Html;
$colors = ['red','green','orange','blue','purple'];
?>
    <div class="tile <?= $colors[$index%5] ?>">
        <?php //echo '$key: '.$key.', $index: '.$index.', mod 4: '.($index%4).'<br />'; ?>
        <h3 class="title"><?= $model->status>0 ? '<span class="glyphicon glyphicon-export" title="'.Yii::t('docvault','Currently checked out').'"></span>&nbsp;' : '' ?><?= Html::encode($model->description) ?></h3>
        <?= Html::a('&raquo; ' . Html::encode($model->realname), ['view', 'id' => $model->id], ['title'=>Yii::t('docvault', 'View document {realname}', ['realname'=>$model->realname])]); ?><br />
        <?= Html::encode(Yii::t('docvault','Category:')).' '.Html::a($model->category->name, ['document-category/view', 'id'=>$model->categoryId], ['title'=>Yii::t('docvault','View documents in category {category}', ['category'=>$model->category->name])]) ?><br />
        <small><?= Html::encode(Yii::t('docvault', 'document created on {created} by {username}', ['created'=>Yii::$app->formatter->asDatetime($model->created), 'username'=>$model->ownedByUser->username])) ?></small>
    </div>
