<?php
use yii\helpers\Html;
?>

<?php // yii\helpers\VarDumper::dump($model->permissions, 10, true); ?>

<table class="table table-striped table-bordered">
    <thead>
        <th><?= Html::encode(Yii::t('docvault', 'User')) ?></th>
        <th><?= Html::encode(Yii::t('docvault', 'Rights')) ?></th>
    </thead>
    <tbody>
        <?php foreach($model->permissions as $permission) : ?>
        <tr>
            <td><?= !is_null($permission->user) ? $permission->user->username : Yii::t('docvault','All Users') ?></td><td><?= /* $permission->rights . ' | ' . */ $permission->rightsOptions[$permission->rights] ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

