<?php
// \yii\helpers\BaseVarDumper::dump($permissions, 10, true);

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Permission;
use Da\user\Model\User;

$this->title = Yii::t('docvault', 'Update Document Permissions:');
$this->params['breadcrumbs'][] = ['label' => 'DocVault', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('docvault', 'Documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $document->id, 'url' => ['view', 'id' => $document->id]];
$this->params['breadcrumbs'][] = Yii::t('docvault', 'Update Permissions');

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="document-view">

	<?php $form = ActiveForm::begin([
		'id' => 'document-permissions-form',
		'layout' => 'horizontal',
		'enableClientValidation' => true,
		'errorSummaryCssClass' => 'error-summary alert alert-danger',
		'fieldConfig' => [
			'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
			'horizontalCssClasses' => [
				'label' => 'col-sm-2',
				#'offset' => 'col-sm-offset-4',
				'wrapper' => 'col-sm-8',
				'error' => '',
				'hint' => '',
			],
		],
	]);
	?>
	<?php 
	$userOptions = ArrayHelper::map(User::find()->all(), 'id', 'username');
	$userOptions = ArrayHelper::merge([0=>Yii::t('docvault','All Users')], $userOptions);
	// DEBUG yii\helpers\VarDumper::dump($userOptions, 10, true);
	?>

	<?= $form->field($document, 'realname')->textInput(['disabled'=>'disabled']) ?>

	<?= $form->field($model, 'viewUsers')->dropDownList($userOptions, ['multiple'=>'multiple']); ?>

	<?= $form->field($model, 'modifyUsers')->dropDownList($userOptions, ['multiple'=>'multiple']); ?>


	<?= Html::submitButton(Yii::t('docvault', 'Update'), ['class' => 'btn btn-success']) ?>


	<?php ActiveForm::end(); ?>

</div>
