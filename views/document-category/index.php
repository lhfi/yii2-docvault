<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DocumentCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('docvault','Document Categories');
$this->params['breadcrumbs'][] = ['label' => 'DocVault', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('docvault','Create New Document Category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'pager' => [
			'class' => yii\widgets\LinkPager::className(),
			'firstPageLabel' => Yii::t('cruds', 'First'),
			'lastPageLabel' => Yii::t('cruds', 'Last'),
		],
		'filterModel' => $searchModel,
		'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
		'headerRowOptions' => ['class'=>'x'],
        'columns' => [
            ['class' => 'yii\grid\ActionColumn', 'template'=>'{view} {update} {delete}'],
            //'id',
            'name',
            [
                'label'=>Yii::t('docvault','Documents'),
                'format' => 'html',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a(Yii::t('docvault', '{n, plural, =0{No documents} =1{One document} other{# documents}}', ['n' => count($model->documents)]), ['document/index', 'DocumentSearch[categoryId]'=>$model->id]);
                },
                'contentOptions' => ['style'=>'text-align:center'],
            ],
        ],
    ]); ?>

</div>
