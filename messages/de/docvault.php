<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Dokument' => 'Dokument',
    '{n, plural, =0{No documents} =1{One document} other{# documents}}' => '{n, plural, =0{Keine Dokumente} =1{Ein Dokument} other{# Dokumente}}',
    '(Select a Category)' => '(Kategorie auswählen)',
    '(Select an Owner )' => '(Eigentümer auswählen)',
    '(Select)' => '(Auswählen)',
    '(not yet)' => '(noch nicht)',
    'Add new users.' => 'Neue Benutzer hinzufügen.',
    'Advanced Search' => 'Erweiterte Suche',
    'All Users' => 'Alle Benutzer',
    'Are you sure you want to delete this item?' => 'Wollen Sie diesen Eintrag wirklich löschen?',
    'Author\'s Note' => 'Verfasser Bemerkungen',
    'Categories' => 'Kategorien',
    'Category' => 'Kategorie',
    'Category:' => 'Kategorie:',
    'Change user settings.' => 'Benutzereinstellungen ändern.',
    'Check In' => 'Einchecken',
    'Check In Date' => 'Datum Check-In',
    'Check Out' => 'Auschecken',
    'Check in, Check out documents.' => 'Dokumente ein- und auschecken.',
    'Checked out by' => 'Ausgecheckt von',
    'Checkin Document' => 'Dokument enchecken',
    'Checkout Document' => 'Dokument auschecken',
    'Control Your Documents.' => 'Kontrollieren Sie Ihre Dokumente.',
    'Create' => 'Anlegen',
    'Create Document Category' => 'Dokumenten-Kategorie anlegen',
    'Create New Document' => 'Neues Dokument anlegen',
    'Create New Document Category' => 'Neue Dokumenten-Kategorie anlegen',
    'Currently checked out' => 'Momentan ausgecheckt',
    'Date First Checked-In' => 'Datum erster Check-In',
    'Date of revision' => 'Datum Revision',
    'Delete' => 'Löschen',
    'Delete Document' => 'Dokument löschen',
    'Description of Contents' => 'Beschreibung Inhalt',
    'Description of modification' => 'Beschreibung Änderungen',
    'Document' => 'Dokument',
    'Document Categories' => 'Dokumenten-Kategorien',
    'Document ID' => 'Dokument-ID',
    'Documents' => 'Dokumente',
    'File' => 'Datei',
    'History' => 'Historie',
    'Home' => 'Home',
    'ID' => 'ID',
    'ID (PK)' => 'ID (PK)',
    'ID of user modifying file' => 'ID des ändernden Benutzers',
    'Manage Categories' => 'Kategorien verwalten',
    'Manage Document Categories' => 'Dokumenten-Kategorien verwalten.',
    'Manage Documents' => 'Dokumente verwalten',
    'Manage Users' => 'Benutzer verwalten',
    'Manage application users.' => 'Anwendungsbenutzer verwalten.',
    'Modify' => 'Ändern',
    'Name' => 'Name',
    'New Document' => 'Neues Dokument',
    'Notes' => 'Bemerkungen',
    'Original File Name' => 'Original-Dateiname',
    'Owner' => 'Eigentümer',
    'Permissions' => 'Berechtigungen',
    'Remove' => 'Entfernen',
    'Reset' => 'Zurücksetzen',
    'Rights' => 'Rechte',
    'Search' => 'Suchen',
    'Search Documents' => 'Dokumente suchen',
    'Show all' => 'Alle zeigen',
    'The document category {name} has been created.' => 'Die Dokumenten-Kategorie {name} wurde angelegt.',
    'The document category {name} has been updated.' => 'Die Dokumenten-Kategorie {name} wurde aktualisiert.',
    'The document {realname} has been checked in.' => 'Das Dokument {realname} wurde eingecheckt.',
    'The document {realname} has been checked out.' => 'Das Dokument {realname} wurde ausgecheckt.',
    'The document {realname} has been created.' => 'Das Dokument {realname} wurde angelegt.',
    'The document {realname} has been deleted.' => 'Das Dokument {realname} wurde gelöscht.',
    'The document {realname} has been updated.' => 'Das Dokument {realname} wurde aktualisiert.',
    'The permissions have been updated.' => 'Die Berechtigungen wurden aktualisiert.',
    'The requested document is currently checked out.' => 'Das angeforderte Doument ist momentan ausgecheckt.',
    'The requested document {realname} is currently checked out.' => 'Das angeforderte Dokument {realname} ist momentan ausgecheckt.',
    'Type' => 'Typ',
    'Update' => 'Aktualisieren',
    'Update Document' => 'Dokument bearbeiten',
    'Update Document Category' => 'Dokumenten-Kategorie bearbeiten',
    'Update Document Permissions' => 'Dokumenten-Berechtigungen bearbeiten',
    'Update Document Permissions:' => 'Dokumenten-Berechtigungen bearbeiten:',
    'Update Document:' => 'Dokument bearbeiten:',
    'Update Permissions' => 'Berechtigungen bearbeiten',
    'Upload New Document' => 'Neues Dokument hochladen',
    'Upload new documents.' => 'Neue Dokumente hochladen.',
    'User' => 'Benutzer',
    'Users' => 'Benutzer',
    'Users with Modify Rights' => 'Benutzer mit Änderungsrechten',
    'Users with View Rights' => 'Benutzer mit Leserechten',
    'View' => 'Anzeigen',
    'View document {realname}' => 'Dokument {realname} zeigen',
    'View documents in category {category}' => 'Dokumente in Kategorie {category} zeigen',
    'View documents.' => 'Dokumente anzeigen.',
    'You are not the owner of the requested document {realname}.' => 'Sie sind nicht der Eigentümer des angeforderten Dokuments {realname}.',
    'You are not the owner of the requested document.' => 'Sie sind nicht der Eigentümer des angeforderten Dokuments.',
    'You don\'t have the permission to delete the document {realname}.' => 'Sie haben keine Berechtigungen zum Löschen des Dokument {realname}.',
    'You don\'t have the permission to modify the document {realname}.' => 'Sie haben keine Berechtigungen zum Ändern des Dokument {realname}.',
    'You don\'t have the permission to view the document {realname}.' => 'Sie haben keine Berechtigungen zum Anzeigen des Dokument {realname}.',
    'document created on {created} by {username}' => 'Dokument angelegt am {created} von {username}',
];
