Installation
============

This document will guide you through the process of installing Yii2-docvault using **composer**. Installation is a quick and easy three-step process.

Step 1: Download Yii2-docvault using composer
---------------------------------------------

Add `"diggindata/yii2-docvault": "0.0.*@dev"` to the require section of your **composer.json** file and run
`composer update` to download and install Yii2-docvault.

Step 2: Configure your application
------------------------------------

Add following lines to your main configuration file:

```php
'modules' => [
    'docvault' => [
        'class' => 'diggindata\docvault\Module',
        'dataDir' => '@app/data', // Alias to data directory
    ],
],
```

Step 3: Update database schema
------------------------------

> **NOTE:** Make sure that you have properly configured **db** application component.

After you downloaded and configured Yii2-docvault, the last thing you need to do is updating your database schema by applying the migrations: 

```bash
$ php yii migrate/up --migrationPath=@vendor/diggindata/yii2-docvault/migrations
```

FAQ
---

**Installation failed. There are no files in `vendor/diggindata/yii2-docvault`**

*Try removing Yii2-docvault version constraint from composer.json, then run `composer update`. After composer finished removing of Yii2-docvault, re-add version constraint and `composer update` again.*

