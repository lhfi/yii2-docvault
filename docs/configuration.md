Configuration
=============

> Note: This section is under development.

* Add following lines to your main configuration file:

```php
'modules' => [
    'docvault' => [
        'class' => 'diggindata\docvault\Module',
        'dataDir' => '@app/data', // Alias to data directory
    ],
],
```
> Note: Make sure the directory is writeable for your webserver.
