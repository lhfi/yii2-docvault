<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace diggindata\docvault;

use yii\base\BootstrapInterface;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\i18n\PhpMessageSource;
use yii\web\GroupUrlRule;
use yii\console\Application as ConsoleApplication;

/**
 * Bootstrap class registers module and docvault application component. It also creates some url rules which will be applied
 * when UrlManager.enablePrettyUrl is enabled.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class Bootstrap implements BootstrapInterface
{
    /** @var array Model's map */
    private $_modelMap = [
        'Document'                  => 'diggindata\docvault\models\Document',
        'DocumentSearch'            => 'diggindata\docvault\models\DocumentSearch',
        'DocumentCategory'          => 'diggindata\docvault\models\DocumentCategory',
        'DocumentCategorySearch'    => 'diggindata\docvault\models\DocumentCategorySearch',
        'DocumentPermissionsForm'   => 'diggindata\docvault\models\DocumentPermissionsForm',
        'Log'                       => 'diggindata\docvault\models\Log',
        'Permission'                => 'diggindata\docvault\models\Permission',
    ];

    /** @inheritdoc */
    public function bootstrap($app)
    {
        /** @var $module Module */
        if ($app->hasModule('docvault') && ($module = $app->getModule('docvault')) instanceof Module) {
            $this->_modelMap = array_merge($this->_modelMap, $module->modelMap);
            foreach ($this->_modelMap as $name => $definition) {
                $class = "diggindata\\docvault\\models\\" . $name;
                \Yii::$container->set($class, $definition);
                $modelName = is_array($definition) ? $definition['class'] : $definition;
                $module->modelMap[$name] = $modelName;
                if (in_array($name, ['Document', 'DocumentCategory', 'Log', 'Permission'])) {
                    \Yii::$container->set($name . 'Query', function () use ($modelName) {
                        return $modelName::find();
                    });
                }
            }
            /*
            \Yii::$container->setSingleton(Finder::className(), [
                'documentQuery'    => \Yii::$container->get('documentQuery'),
                'documentCategoryQuery'    => \Yii::$container->get('documentCategoryQuery'),
            ]);
            */

            if ($app instanceof ConsoleApplication) {
                $module->controllerNamespace = 'diggindata\docvault\commands';
            } else {

                $configUrlRule = [
                    'prefix' => $module->urlPrefix,
                    'rules'  => $module->urlRules
                ];

                if ($module->urlPrefix != 'docvault') {
                    $configUrlRule['routePrefix'] = 'docvault';
                }

                $app->get('urlManager')->rules[] = new GroupUrlRule($configUrlRule);

                if (!$app->has('authClientCollection')) {
                    $app->set('authClientCollection', [
                        'class' => Collection::className(),
                    ]);
                }
            }

            $app->get('i18n')->translations['docvault*'] = [
                'class'    => PhpMessageSource::className(),
                'basePath' => __DIR__ . '/messages',
            ];

            $defaults = [
                'welcomeSubject'        => \Yii::t('app', 'Welcome to {0}', \Yii::$app->name),
                'confirmationSubject'   => \Yii::t('app', 'Confirm account on {0}', \Yii::$app->name),
                'reconfirmationSubject' => \Yii::t('app', 'Confirm email change on {0}', \Yii::$app->name),
                'recoverySubject'       => \Yii::t('app', 'Complete password reset on {0}', \Yii::$app->name)
            ];

            //\Yii::$container->set('dektrium\user\Mailer', array_merge($defaults, $module->mailer));
        }
        
    }
}
